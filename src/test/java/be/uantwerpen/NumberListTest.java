package be.uantwerpen;

import org.junit.Assert;
import org.junit.Test;

import sun.security.krb5.internal.ccache.FileCredentialsCache;

import java.util.ArrayList;

public class NumberListTest {
    @Test
    public void testGiveNumberPosition() {

        NumberList list = new NumberList("numbersFromZeroToTwenty");

        int n = 20;
	    for (int i = 0; i <= n; i++) {
            list.members.add(i,i+20);
            }
        
        Assert.assertEquals(0, list.giveNumberPosition(20));
        Assert.assertEquals(11, list.giveNumberPosition(31));
        Assert.assertEquals(20, list.giveNumberPosition(40));

    }

    @Test
    public void testGiveNumberInclusion() {

        NumberList list = new NumberList("numbersFromZeroToTwenty");

        int n = 20;
	    for (int i = 0; i <= n; i++) {
            list.members.add(i,i+20);
            }
        
        Assert.assertEquals(true, list.giveNumberInclusion(20));
        Assert.assertEquals(true, list.giveNumberInclusion(27));
        Assert.assertEquals(false, list.giveNumberInclusion(55));

    }
}
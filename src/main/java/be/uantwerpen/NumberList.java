package be.uantwerpen;

import java.util.ArrayList; //Om een ArrayList te gebruiken, moet je de library klasse die Java voorziet importeren

public class NumberList implements NumberListIf {
    public String name;
    public ArrayList<Integer> members; //We definiëren een ArrayList met als type van de elementen Integers, en met "members" als verwijzing naar de ArrayList

    public NumberList(String name) { //Constructor
        this.name = name;
        this.members = new ArrayList<Integer>();
    }

    public int giveNumberPosition(int inputNumber) {
        Integer result = null;
        for(int member : members) { //Een for-loop uitgevoerd over alle elementen in een ArrayList
            if(member == inputNumber) {
                result = members.indexOf(member);
            }
        }
        return result;
    }

    public boolean giveNumberInclusion(int inputNumber) {
        boolean result = false;
        for(int member : members) {
            if(member == inputNumber) {
                result = true;
            }
        }
        return result;
    }
}
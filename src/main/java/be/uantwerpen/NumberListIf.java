package be.uantwerpen;

import java.util.ArrayList;

public interface NumberListIf {

    public int giveNumberPosition(int inputNumber);
    public boolean giveNumberInclusion(int inputNumber);
}
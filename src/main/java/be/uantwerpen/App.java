package be.uantwerpen;

import be.uantwerpen.NumberList;
import be.uantwerpen.NumberListIf;

public class App {
    public static void main(String[] args) {
        NumberList list = new NumberList("numbersFromTwentyToForty");

        int n = 20;
	    for (int i = 0; i <= n; i++) {
            list.members.add(i+20);
            }

            System.out.println("The position of the number 24 in the ArrayList is: " + list.giveNumberPosition(24));
            System.out.println("Is the number 29 included in the ArrayList?: " + list.giveNumberInclusion(29));
            System.out.println("Is the number 6 included in the ArrayList?: " + list.giveNumberInclusion(6));


    }
}

# Opgave NumberList oefening#

In deze oefening zullen we ons vertrouwd maken met een belangrijk concept in Java, een **ArrayList**. Meer informatie over het gebruik van dit construct in Java is terug te vinden op onder andere: https://www.tutorialspoint.com/java/java_arraylist_class.htm. Daar kan je terugvinden hoe een constructor van een ArayList er uit ziet en welke methoden er op een ArrayList kunnen worden uitgevoerd.

In deze eenvoudige implementatie, gebruiken we een ArrayList om de cijfers van 20 tot en met 40 bij te houden op posities 0 tot en met 20 in de ArrayList. 

We willen kunnen opzoeken op welke locatie een bepaald cijfer is opgeslagen in de ArrayList, en of een bepaalde cijfer al dan niet opgeslagen is in de lijst. Deze functionaliteit is voorgeschreven in de **NumberListIf** interfaceklasse, waaraan niets veranderd moet worden.

Schrijf zelf een methode om de volledige ArrayList te overlopen en terug te geven of een bepaald nummer zich in de ArrayList bevindt.

### Functionaliteit van applicatie ###

De code dient de volgende functionaliteit te voldoen/voorzien (deze functionaliteit wordt gecontroleerd in de tests, maar kan ook zelf geprogrammeerd worden in de App.java klasse):

* Een instantie aanmaken van de klassen "NumberList" met alle benodigde attributen
* Een ArrayList kunnen vullen met elementen die de waarde 20 t/m 40 hebben
* Kunnen opzoeken waar een bepaald element is opgeslagen in een ArrayList
* Kunnen opzoeken of een bepaald element al dan niet is opgeslagen in een ArrayList

### Leerdoelen van oefening ###

* Vertrouwd geraken met het concept ArrayList
* Een methode schrijven die alle elementen van een ArrayList overloopt om te zoeken of een element zich in de ArrayList bevindt

### Workflow oefeningen ###

- Maak een fork van een repository van de opdracht
    - Log in op Bitbucket met jouw @student.uantwerpen.be account
    - Ga naar de Bitbucket url van de oefening die op Blackboard geplaatst is
    - Klik links op "+" en dan op "Fork this repository"
- Geef in BitBucket read-access geven aan de begeleiders voor de geforkte repository
    - Ga in je eigen repository naar Settings -> User and group access -> voeg één voor één "phuysmans" en "gillesoorts" als Users toe, geef "read" access, klik op "Add".
- Navigeer in de command line naar de folder waar je de oefening op jouw lokale schijf wil opslaan
- Clone de geforkte repository naar een lokale repository
    - Gebruik de "clone" knop rechts bovenaan op de repository webpagina op Bitbucket om het commando te krijgen, bvb.: "git clone https://gillesoorts@bitbucket.org/phuysmans/programmeren_ex-helloworld-java.git"
    - Geef dit commando in de command line in
- Open de oefeningenfolder in een editor waarin je zal programmeren. Indien dit VS Code is, kan je dit via command line snel doen met het commando "code ."
- De opgave van de oefening vind je terug in het bestand readme.md in de hoofdfolder van de repository. De inhoud van dit bestand wordt ook automatisch weergegeven op de "Source" pagina van de repository op de BitBucket website. De codebestanden vind je in de "src" subfolders.
- Maak aanpassingen in de code om de assignment op te lossen.
- Sla alle veranderingen in bronbestanden steeds op voordat je opnieuw compileert en test!
- Compileer en test de geschreven code met Maven. Voer dit in één commando uit vanuit de hoofddirectory van de oefeningenfolder (dit is de folder waarin zich het pom.xml bestand bevindt):
```
mvn test
```
Wanneer deze test niet correct verloopt, kijk je eerst naar de compilatiefouten (bij een "BUILD FAILURE") om deze in de code te verbeteren, dit zullen voornamelijk spellingsfouten zijn en fouten tegen de Java syntax. Compileert de code correct, maar krijg je geen succesvol testresultaat, is er een fout in de logica van de code waardoor deze niet het juiste resultaat geeft. Ga na aan de hand van het "mvn compile exec:java" commando wat het resultaat is van de code, en corrigeer aan de hand van deze output de code.
Mocht dit niet lukken en krijg je maar geen geslaagde test, dien je code dan toch zeker in via de volgende stappen, de oefeningen worden immers gequoteerd op de kwaliteit van de code. Jouw problemen worden besproken in de volgende oefeningenles.

- Voeg alle broncode-bestanden die je aangepast of aangemaakt hebt toe aan de Git staging area. Let er op dat je dit doet in de directory waarin de bestanden zich bevinden, of dat je de relatieve bestandslocatie vanaf de huidige directory meegeeft: 
```
git add <bestandslocatie/bestandsnaam.java>
```
- Commit de veranderingen aan de repository via:
```
git commit -m "Omschrijving commit"
```
- Stuur de lokale commits naar de remote repository:
```
git push
```

Met de laatste git push, heb je de oefening succesvol ingediend.